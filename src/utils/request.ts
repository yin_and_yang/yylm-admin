// import notification from 'ant-design-vue/es/notification'
import { ACCESS_TOKEN } from '../store/mutation-types'
import { Result } from './typings'

const request = async <T = any>(url: string, config: RequestInit) => {
  const baseURL = import.meta.env.VITE_API_BASE_URL as string | undefined
  const token = localStorage.getItem(ACCESS_TOKEN)
  if (token) {
    config.headers = {
      ...config.headers,
      'Access-Token': token
    }
  }

  const response = await fetch(baseURL + url, config)

  if (!response.ok) {
    return Promise.reject('请求异常')
  }
  const result: Result<T> = await response.json()
  if (result.code !== 200) {
    return Promise.reject(result.msg)
  }

  return result
}

// fetch('', {})

export default request

// // 创建 axios 实例
// const request = axios.create({
//   // API 请求的默认前缀
//   baseURL: import.meta.env.VITE_API_BASE_URL as string | undefined,
//   timeout: 6000 // 请求超时时间
// })

// // 异常拦截处理器
// const errorHandler = (error: any) => {
//   notification.error({
//     message: '异常',
//     description: '请求异常'
//   })
//   return Promise.reject(error)
// }

// // request interceptor
// request.interceptors.request.use(config => {
//   const token = localStorage.getItem(ACCESS_TOKEN)
//   // 如果 token 存在
//   // 让每个请求携带自定义 token 请根据实际情况自行修改
//   if (token) {
//     config.headers['Access-Token'] = token
//   }
//   return config
// }, errorHandler)

// // response interceptor
// request.interceptors.response.use(response => {
//   return response.data
// }, errorHandler)

// export default request
