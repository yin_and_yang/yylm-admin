export type Result<T = any> = {
  code: number
  status: 'success' | 'error' | 'warning'
  msg: string
  data: T
}
