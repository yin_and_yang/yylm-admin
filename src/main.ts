import { createApp } from 'vue'
import App from './App.vue'
import './global.less' // global style
import './permission' // permission control
import 'nprogress/nprogress.css'
// import { registerRouter },  from './router'
import router from './router'
import './store' // init store
import './mock'
import registerNaiveUi from './config/naiveui.register'

const app = createApp(App)
app.use(router)

registerNaiveUi(app)

app.mount('#app')
