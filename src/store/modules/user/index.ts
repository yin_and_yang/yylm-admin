import { UserReducer, UserState } from '../../types/store'

const userState: UserState = {
  id: 0,
  name: ''
}

const counterReducer: UserReducer = function (state = userState, action) {
  switch (action.type) {
    case 'user/modify':
      return { ...state, ...action.payload }
    case 'user/reset':
      return { ...userState }
    default:
      return state
  }
}

export default counterReducer

// export const getUserRef = () => {
//   const userRef = ref(userStore.getState().value)
//   userStore.subscribe(() => {
//     userRef.value = userStore.getState().value
//   })
//   return userRef
// }
