import { AppReducer, AppState } from '../../types/store'
import { APP_MODIFY } from './actionEnum'

const appState: AppState = { sideCollapsed: false }

const counterReducer: AppReducer = function (state = appState, action) {
  switch (action.type) {
    case APP_MODIFY:
      return { ...state, ...action.payload }
    default:
      return state
  }
}

export default counterReducer
