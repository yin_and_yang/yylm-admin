import { StoreAction } from '../../types/store'
import { APP_MODIFY } from './actionEnum'

export function modifySideCollapsed(sideCollapsed: boolean): StoreAction {
  return {
    type: APP_MODIFY,
    payload: {
      sideCollapsed
    }
  }
}
