import { Action } from 'redux'
import { ThunkAction } from 'redux-thunk'

export type StoreAction = Action<string | Symbol> & { payload?: any }

export type ThunkResult<R> = ThunkAction<R, State, undefined, StoreAction>

export type State = {
  app: AppState
  user: UserState
}

export interface AppReducer {
  (state: AppState | undefined, action: StoreAction): AppState
}
export interface UserReducer {
  (state: UserState | undefined, action: StoreAction): UserState
}

export type StateReducer = {
  app: AppReducer
  user: UserReducer
}

export type AppState = {
  sideCollapsed: boolean
}

export type UserState = {
  id: number
  name: string
}
