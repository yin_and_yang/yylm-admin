import { ComponentPublicInstance } from 'vue'
export type Pagination = {
  page: number
  limit?: number
}

export type LoadDataFunction = (parameter: Pagination) => Promise<any>

export type STableInstance = ComponentPublicInstance<{
  refresh: (bool = false) => void
}>
