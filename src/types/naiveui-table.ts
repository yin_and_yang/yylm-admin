import { TableBaseColumn } from 'naive-ui/lib/data-table/src/interface'

export type TableColumn = TableBaseColumn & {
  exportWhiteList?: boolean
}
