type MockRequest<T = any> = {
  method: string
  body: T
}

type MockResponse<T = any> = {
  code: number
  msg: string
  data: T
}

export type MockResponseFunction<req = any, res = any> = (url: string, opts: MockRequest<req>) => MockResponse<res>
