// import { MockRequest } from './types/mock'

export const builder = (data: any, msg?: string, code?: number) => {
  return {
    code: code || 200,
    msg: msg ?? '成功',
    data
  }
}

// export const getBody = <T = any>(options: MockRequest) => {
//   return options.body && <T>JSON.parse(options.body)
// }
