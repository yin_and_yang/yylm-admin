import fetchMock from 'fetch-mock/esm/client'

// 将未捕获的请求导向网络
fetchMock.config.fallbackToNetwork = true

// 动态批量导入services的模块
const services = import.meta.glob('./services/*.ts')
Object.values(services).forEach(module => module())
