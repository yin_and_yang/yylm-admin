import fetchMock from 'fetch-mock/esm/client'
import Mock from 'mockjs'
import { MockResponseFunction } from '../types/mock'
import { builder } from '../util'

const tableMock: MockResponseFunction = (url, config) => {
  const { page, limit } = config.body ?? { page: 1, limit: 10 }
  const structureData = (limit: number, page: number) => {
    const data = Array.from(new Array(limit), (item, index) => {
      return {
        id: index + 1 + (page - 1) * limit,
        name: Mock.Random.cname(),
        age: Mock.Random.integer(1, 100),
        ...Mock.mock({
          'sex|1': ['男', '女']
        })
      }
    })
    return data
  }

  return builder({
    page,
    limit,
    total: 500,
    pageCount: Math.ceil(500 / limit),
    data: structureData(limit, page)
  })
}

fetchMock.post(/\/api\/list\/table/, tableMock, {
  delay: 500
})
