import fetchMock from 'fetch-mock/esm/client'
import Mock from 'mockjs'
import { MockResponseFunction } from '../types/mock'
import { builder } from '../util'

const info: MockResponseFunction = () => {
  return builder({
    id: Mock.Random.id(),
    name: Mock.Random.cname(),
    token: Mock.Random.string()
  })
}

fetchMock.post(/\/api\/auth\/login/, info)
