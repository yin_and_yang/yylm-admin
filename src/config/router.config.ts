import { BasicLayout } from '../layouts'
import RouteView from '@/layouts/RouteView.vue'
import { RouterMap } from '@/router/typings'
import { Component, h } from 'vue'
import { NIcon } from 'naive-ui'
import { DashboardFilled } from '@vicons/antd'

function renderIcon(icon: Component) {
  return () => h(NIcon, null, { default: () => h(icon) })
}

export const asyncRouterMap: RouterMap[] = [
  {
    path: '/',
    name: 'index',
    component: BasicLayout,
    meta: { title: '主页' },
    redirect: '/dashboard',
    children: [
      {
        path: '/dashboard',
        name: 'dashboard',
        component: () => import('@/views/dashboard/Workplace.vue'),
        meta: { title: '数据看板', icon: renderIcon(DashboardFilled) }
      },
      // forms
      {
        path: '/form',
        redirect: '/form/base-form',
        component: RouteView,
        name: 'Form',
        meta: { title: '表单', icon: renderIcon(DashboardFilled), permission: ['form'] },
        children: [
          {
            path: '/form/base-form',
            name: 'BaseForm',
            component: () => import('@/views/form/BasicForm.vue'),
            meta: { title: '简单表单', keepAlive: true, permission: ['form'] }
          },
          {
            path: '/form/step-form',
            name: 'StepForm',
            component: () => import('@/views/form/StepForm.vue'),
            meta: { title: '步进表单', keepAlive: true, permission: ['form'] }
          },
          {
            path: '/form/advanced-form',
            name: 'AdvanceForm',
            component: () => import('@/views/form/AdvancedForm.vue'),
            meta: { title: '高级表单', keepAlive: true, permission: ['form'] }
          }
        ]
      },
      // list
      {
        path: '/list',
        redirect: '/list/table-list',
        component: RouteView,
        name: 'List',
        meta: { title: '列表', icon: renderIcon(DashboardFilled), permission: ['list'] },
        children: [
          {
            path: '/list/table-list',
            name: 'TableList',
            component: () => import('@/views/list/table-list/TableList.vue'),
            meta: { title: '查询表格', keepAlive: true, permission: ['list'] }
          },
          {
            path: '/list/basic-list',
            name: 'BasicList',
            component: () => import('@/views/list/basic-list/BasicList.vue'),
            meta: { title: '标准列表', keepAlive: true, permission: ['list'] }
          },
          {
            path: '/list/card',
            name: 'CardList',
            component: () => import('@/views/list/card-list/CardList.vue'),
            meta: { title: '卡片列表', keepAlive: true, permission: ['list'] }
          },
          {
            path: '/list/search',
            name: 'SearchList',
            component: RouteView,
            meta: { title: '搜索列表', keepAlive: true, permission: ['list'] },
            redirect: '/list/search/article',
            children: [
              {
                path: '/list/search/article',
                name: 'SearchArticle',
                component: () => import('@/views/form/AdvancedForm.vue'),
                meta: { title: '搜索列表（文章）', keepAlive: true, permission: ['list'] }
              }
            ]
          }
        ]
      }
    ]
  },
  {
    path: '/:pathMatch(.*)*',
    redirect: '/404',
    hidden: true
  }
]

/**
 * 基础路由
 * @type { *[] }
 */

export const constantRouterMap: RouterMap[] = [
  {
    path: '/user/login',
    name: 'login',
    component: () => import('@/views/user/Login.vue')
  },
  {
    path: '/404',
    component: () => import('@/views/exception/404.vue')
  },
  ...asyncRouterMap
]
