import { App } from 'vue'
import {
  // create naive ui
  create,
  // component
  NButton,
  NConfigProvider,
  NMessageProvider,
  NTabs,
  NTabPane,
  NSpace,
  NSwitch,
  NMenu,
  NLayout,
  NLayoutSider,
  NLayoutHeader,
  NLayoutContent,
  NIcon,
  NCard,
  NPageHeader,
  NBreadcrumb,
  NBreadcrumbItem,
  NDataTable,
  NInput,
  NGi,
  NGrid,
  NGridItem,
  NDrawer,
  NDrawerContent
} from 'naive-ui'

const naive = create({
  components: [
    NButton,
    NConfigProvider,
    NMessageProvider,
    NTabs,
    NTabPane,
    NSpace,
    NSwitch,
    NLayout,
    NLayoutSider,
    NLayoutHeader,
    NLayoutContent,
    NMenu,
    NIcon,
    NCard,
    NPageHeader,
    NBreadcrumb,
    NBreadcrumbItem,
    NDataTable,
    NInput,
    NGi,
    NGrid,
    NGridItem,
    NDrawer,
    NDrawerContent
  ]
})

const registerNaiveUi = (app: App<Element>) => {
  app.use(naive)
}

export default registerNaiveUi
