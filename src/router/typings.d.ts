import { VNodeChild } from '@vue/runtime-core'
import { RouteRecordRaw } from 'vue-router'
import 'vue-router'
declare module 'vue-router' {
  interface RouteMeta {
    // 声明路由元信息
    title: string
    icon?: () => VNodeChild
  }
}

export type RouterMap = RouteRecordRaw & { hidden?: boolean; children?: RouterMap[] }
