import { constantRouterMap } from '../config/router.config'
import * as VueRouter from 'vue-router'
// import { App } from 'vue'

const router = VueRouter.createRouter({
  // 4. 内部提供了 history 模式的实现。为了简单起见，我们在这里使用 hash 模式。
  // VueRouter.createWebHistory(),
  history: VueRouter.createWebHistory(),
  routes: constantRouterMap // `routes: routes` 的缩写
})

// export function registerRouter(app: App<Element>) {
//   app.use(router)
// }

export default router
