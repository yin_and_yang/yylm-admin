import request from '../utils/request'

const userApi = {
  table: '/list/table'
}

export function tableList<T = any>(data?: any) {
  return request<T>(userApi.table, {
    method: 'post',
    body: data
  })
}
