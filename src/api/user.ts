import request from '../utils/request'

const userApi = {
  Login: '/auth/login'
}

export function login<T = any>(data: any) {
  return request<T>(userApi.Login, {
    method: 'post',
    body: data
  })
}
