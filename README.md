# Yylm-admin

Yylm-admin 是一个免费开源的中后台模版。使用了最新的`vue3`,`vite2`,`TypeScript`,`naiveui`等主流技术开发，开箱即用的中后台前端解决方案，也可用于学习参考。

## 特性

- **最新技术栈**：使用 Vue3/vite2 等前端前沿技术开发
- **TypeScript**: 应用程序级 JavaScript 的语言
- **Naiveui**: 尤大推荐的vue3 UI库
- **Mock 数据** 内置 Mock 数据方案

### 准备

- [node](http://nodejs.org/) 和 [git](https://git-scm.com/) -项目开发环境
- [Vite](https://vitejs.dev/) - 熟悉 vite 特性
- [Vue3](https://v3.vuejs.org/) - 熟悉 Vue 基础语法
- [TypeScript](https://www.typescriptlang.org/) - 熟悉`TypeScript`基本语法
- [Es6+](http://es6.ruanyifeng.com/) - 熟悉 es6 基本语法
- [Vue-Router-Next](https://next.router.vuejs.org/) - 熟悉 vue-router 基本使用
- [Naive-ui](https://www.naiveui.com/) - ui 基本使用
- [fetch mock](http://www.wheresrhys.co.uk/fetch-mock/) - mockjs 基本语法

###  安装使用

​	

- 获取项目代码

```
git clone https://gitee.com/yin_and_yang/yylm-admin
```

- 安装依赖

```
cd yylm-admin

yarn install
```

- 运行

```
yarn dev
```

- 打包

```
yarn build
```

## License

[MIT © yin_and_yang](https://github.com/anncwb/vue-vben-admin/blob/main/LICENSE)

